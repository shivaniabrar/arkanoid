package com.walchand.impl.gameinterface;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import com.walchand.gameinterface.Audio;
import com.walchand.gameinterface.Music;
import com.walchand.gameinterface.Sound;

public class AAudio implements Audio {

	AssetManager assets;
	SoundPool soundPool;
	
	public AAudio(Activity activity) {
		activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		this.assets = activity.getAssets();
		soundPool = new SoundPool(25, AudioManager.STREAM_MUSIC, 0);
	}
	
	public Music newMusic(String fileName) {	
		
		try {
			AssetFileDescriptor assetFileDescriptor = assets.openFd(fileName);
			return new AMusic(assetFileDescriptor);
		} catch (IOException e) {
			throw new RuntimeException("Cannot Open File");
		}
		
	}

	public Sound newSound(String fileName) {
		try {
			AssetFileDescriptor assetFileDescriptor = assets.openFd(fileName);
			int soundID = soundPool.load(assetFileDescriptor, 1);
			return new ASound(soundPool,soundID);
		} catch (IOException e) {
			throw new RuntimeException("Cannot Open File");
		}
		
	}

}
