package com.walchand.impl.gameinterface;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;

import com.walchand.gameinterface.Audio;
import com.walchand.gameinterface.FileIO;
import com.walchand.gameinterface.Game;
import com.walchand.gameinterface.Graphics;
import com.walchand.gameinterface.Input;
import com.walchand.gameinterface.Screen;



public abstract class AGame extends Activity implements Game {

	ARenderView renderView;
	Graphics graphics;
	Audio audio;
	Input input;
	FileIO fileIO;
	Screen screen;
	WakeLock wakelock;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		boolean isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

		int frameBufferWidth = isLandscape ? 480 : 320;
		int frameBufferHeight = isLandscape ? 320 : 480 ;
		Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
				frameBufferHeight, Config.RGB_565);

		float scaleX = (float) frameBufferWidth
				/ getWindowManager().getDefaultDisplay().getWidth();

		float scaleY = (float) frameBufferHeight
				/ getWindowManager().getDefaultDisplay().getHeight();

		renderView = new ARenderView(this, frameBuffer);
		graphics = new AGraphics(getAssets(), frameBuffer);
		fileIO = new AFileIO(this);
		audio = new AAudio(this);
		input = new AInput(this, renderView, scaleX, scaleY);
		screen = getStartScreen();
		setContentView(renderView);

		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);

		wakelock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,
				"GLGAME");

	}

	
	@Override
	protected void onPause() {
		super.onPause();
		wakelock.release();
		renderView.pause();
		screen.pause();
		
		if(isFinishing())
			screen.dispose();
		
	}


	@Override
	protected void onResume() {
			super.onResume();
			wakelock.acquire();
			screen.resume();
			renderView.resume();
	}


	public Input getInput() {
		return input;
	}

	public FileIO getFileIO() {
		return fileIO;
	}

	public Graphics getGraphics() {
		return graphics;
	}

	public Audio getAudio() {
		return audio;
	}

	public void setScreen(Screen screen) {
		if(screen == null)
			throw new IllegalArgumentException("Screen should not be null");
		this.screen.pause();
		this.screen.dispose();
		screen.resume();
		screen.update(0);
		this.screen = screen;
	}

	public Screen getCurrentScreen() {
		return screen;
	}


}
