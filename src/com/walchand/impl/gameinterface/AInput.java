package com.walchand.impl.gameinterface;

import java.util.List;

import android.content.Context;
import android.os.Build.VERSION;
import android.view.View;

import com.walchand.gameinterface.Input;

public class AInput implements Input {
	
	AcclerometerHandler accelHandler;
	KeyboardHandler keyHandler;
	TouchHandler touchHandler;

	public AInput(Context context,View view,float scaleX,float scaleY) {
		accelHandler = new AcclerometerHandler(context);
		keyHandler = new KeyboardHandler(view);
		if (VERSION.SDK_INT < 5)
			touchHandler = new SingleTouchHandler(view, scaleX, scaleY);
		else
			touchHandler = new MultiTouchHandler(view, scaleX, scaleY);
	}
	public boolean isKeyPressed(int keyCode) {
		return keyHandler.isKeyPressed(keyCode);
	}

	public boolean isTouchDown(int pointer) {	
		return touchHandler.isTouchDown(pointer);
	}

	public int getTouchX(int pointer) {
		return touchHandler.getTouchX(pointer);
	}

	public int getTouchY(int pointer) {
		return touchHandler.getTouchY(pointer);
	}

	public float getAccelX() {
		return accelHandler.getaccelX();
	}

	public float getAccelY() {
		return accelHandler.getaccelY();
	}

	public float getAccelZ() {
		return accelHandler.getaccelZ();
	}

	public List<KeyEvent> getKeyEvents() {
		return keyHandler.getKeyEvents();
	}

	public List<TouchEvent> getTouchEvents() {
		return touchHandler.getTouchEvents();
	}

}
