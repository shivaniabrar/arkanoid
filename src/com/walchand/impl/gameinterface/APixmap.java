package com.walchand.impl.gameinterface;

import android.graphics.Bitmap;

import com.walchand.gameinterface.Pixmap;
import com.walchand.gameinterface.Graphics.PixmapFormat;

public class APixmap implements Pixmap{

	Bitmap bitmap;
	PixmapFormat format;
	
	public APixmap(Bitmap bitmap,PixmapFormat format) {
		this.bitmap = bitmap;
		this.format = format;
	}

	public int getWidth() {
		return bitmap.getWidth();
	}

	public int getHeight() {
		return bitmap.getHeight();
	}

	public PixmapFormat getFormat() {
		return format;
	}

	public void dispose() {
		bitmap.recycle();
	}
	
	
}
