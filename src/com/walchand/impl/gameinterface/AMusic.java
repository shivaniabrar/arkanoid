package com.walchand.impl.gameinterface;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

import com.walchand.gameinterface.Music;

public class AMusic implements Music, OnCompletionListener {

	MediaPlayer mediaPlayer;
	boolean isPrepaid = false;

	public AMusic(AssetFileDescriptor assetFileDescriptor) {
		mediaPlayer = new MediaPlayer();
		try {
			mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(),
					assetFileDescriptor.getStartOffset(),
					assetFileDescriptor.getLength());
			mediaPlayer.prepare();
			isPrepaid = true;
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void play() {

		if (mediaPlayer.isPlaying())
			return;

		try {
			synchronized (this) {
				if (!isPrepaid) {
					mediaPlayer.prepare();
					isPrepaid = true;
				}
				mediaPlayer.start();
			}

		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void pause() {
		if (mediaPlayer.isPlaying())
			mediaPlayer.pause();
	}

	public void stop() {
		mediaPlayer.stop();
		synchronized (this) {
			isPrepaid = false;
		}
	}

	public void setLooping(boolean looping) {
		mediaPlayer.setLooping(looping);
	}

	public void setVolume(float volume) {
		mediaPlayer.setVolume(volume, volume);
	}

	public boolean isPlaying() {
		return mediaPlayer.isPlaying();
	}

	public boolean isStopped() {
		return !isPrepaid;
	}

	public boolean isLooping() {
		return mediaPlayer.isLooping();
	}

	public void dispose() {
		if (mediaPlayer.isPlaying())
			mediaPlayer.stop();
		mediaPlayer.release();
	}

	public void onCompletion(MediaPlayer mp) {
		synchronized (this) {
			if(!mediaPlayer.isPlaying())
				isPrepaid = false;
		}

	}

}
