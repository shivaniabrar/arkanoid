package com.walchand.gameinterface;

import com.walchand.gameinterface.Graphics.PixmapFormat;

public interface Pixmap {

	public int getWidth();
	
	public int getHeight();
	
	public PixmapFormat getFormat();
	
	public void dispose();
}
