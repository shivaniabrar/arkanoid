package com.walchand.gameinterface;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface FileIO {

	
	//Reading Asset (Asset are read-only)  
	public InputStream readAsset(String fileName) throws IOException;
	
	//Read file from external storage
	public InputStream readFile(String fileName) throws IOException;
	
	//Writing file to external storage
	public OutputStream writeFile(String fileName) throws IOException;

}
