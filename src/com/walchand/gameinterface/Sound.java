package com.walchand.gameinterface;

public interface Sound {

	//function to play sound
	public void play(float volume);
	
	//function to dispose the sound
	public void dispose();
}
