package com.walchand.gameinterface;

public interface Music {

	//Function to play music
	public void play();
	
	//Function to pause music
	public void pause();
	
	//Function to stop music
	public void stop();
	
	//Function to set music in loop
	public void setLooping(boolean looping);

	//Function to set volume of music
	public void setVolume(float volume);
	
	//Function to get status of music whether it is playing
	public boolean isPlaying();
	
	//Function to get status of music whether it is stopped
	public boolean isStopped();
	
	//Function to get status of music whether it is looping	
	public boolean isLooping();
	
	//Function to dispose
	public void dispose();
}
