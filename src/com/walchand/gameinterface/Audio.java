package com.walchand.gameinterface;

public interface Audio {

	//Get reference to Music interface
	public Music newMusic(String fileName);
	
	//Get reference to Sound interface
	public Sound newSound(String fileName);
	
}
