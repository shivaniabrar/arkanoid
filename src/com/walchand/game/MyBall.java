package com.walchand.game;

import java.util.List;

import android.net.NetworkInfo.DetailedState;

public class MyBall extends ColloidalObjects {

	// Constants
	public static final short changeDirection = -1;
	public static final short sameDirection = 1;

	private float x_direction_move;
	private float y_direction_move;
	private float x_direction = 1;
	private float y_direction = -1;

	private float last_x;
	private float last_y;
	private float last_xmax;
	private float last_ymax;

	private final static int BALL_WIDTH = 13;
	private final static int BALL_HEIGHT = 12;

	boolean firsttime;
	private float speed_diff;
	// status
	short status;
	

	public MyBall(int x, int y, int gameWidth, int gameHeight) {
		super(x, y, BALL_WIDTH, BALL_HEIGHT, gameWidth, gameHeight);

		speed = 2;
		status = World.NoCollision;
		last_x = x;
		last_y = y;
		last_xmax = xmax;
		last_ymax = ymax;
	}

	// Move Ball
	public short move(Paddle Paddle, List<BRICK> bricks, float deltaTime) {

		speed_diff = speed * deltaTime;
	//	speed_diff = speed;
		status = collisionDetect(Paddle, bricks);

		last_x = x;
		last_y = y;
		last_xmax = xmax;
		last_ymax = ymax;

//		if (status != World.NoCollision || !firsttime) {
			mynextMove();
	//	firsttime = false;
		//}

		moveBall(x_direction_move, y_direction_move);

		if (ymax >= gameHeight)
			status = World.GameOver;

		return status;
	}

	// Calculate next move
	private void mynextMove() {
		x_direction_move = x_direction * speed_diff;
		y_direction_move = y_direction * speed_diff;
	}

	// Detect collision
	private short collisionDetect(Paddle Paddle, List<BRICK> bricks) {

		// Calculate xmax & ymax
		calculate_xmax();
		calculate_ymax();

		if (WallCollision())
			return World.WallCollision;
		if (PaddleCollision(Paddle))
			return World.PaddleCollision;
		if (BrickCollision(bricks))
			return World.BrickCollision;

		return World.NoCollision;
	}

	private boolean WallCollision() {
		float diff;
		if (x >= boundaryX) {
			x_direction *= -1;
			diff = x - boundaryX;
		//	 speed_diff -= diff;
			x += diff * x_direction;
			y += diff * y_direction;
			return true;
		}
		if (x <= 0) {
			x_direction *= -1;
			diff = x * -1;
//			 speed_diff -= diff;
			x += diff * x_direction;
			y += diff * y_direction;
			return true;

		}
		// if (y >= boundaryY) {
		// y_direction *= -1;
		// diff = y - boundaryY;
		// x += diff * x_direction;
		// y += diff * y_direction;
		// return true;
		//
		// }
		if (y <= 0) {
			y_direction *= -1;
			diff = y * -1;
//			 speed_diff -= diff;
			x += diff * x_direction;
			y += diff * y_direction;
			return true;
		}

		return false;
	}

	private boolean PaddleCollision(Paddle obj) {

		float diff;
		if (inBounds(obj)) {
			if (last_x >= obj.xmax) {
				x_direction *= -1;
				diff = x - obj.xmax;
//				 speed_diff -= diff;
				x += diff * x_direction;
				y += diff * y_direction;
				return true;
			}
			if (last_xmax <= obj.x) {
				x_direction *= -1;
				diff = obj.x - x;
//				 speed_diff -= diff;
				x += diff * x_direction;
				y += diff * y_direction;
				return true;
			}
			if (last_y >= obj.ymax) {
				y_direction *= -1;
				diff = y - obj.ymax;
//				 speed_diff -= diff;
				x += diff * x_direction;
				y += diff * y_direction;
				return true;
			}
			if (last_ymax <= obj.y) {
				y_direction *= -1;
				diff = obj.y - y;
//				 speed_diff -= diff;
				x += diff * x_direction;
				y += diff * y_direction;
				return true;
			}
		}

		return false;
	}

	private boolean BrickCollision(List<BRICK> bricks) {
		boolean status = false;
		float diff;
		for (BRICK obj : bricks) {

			if (inBounds(obj)) {
				if (last_x >= obj.xmax) {
					x_direction *= -1;
					diff = x - obj.xmax;
//					 speed_diff -= diff;
					x += diff * x_direction;
					y += diff * y_direction;
				}
				if (last_xmax <= obj.x) {
					x_direction *= -1;
					diff = obj.x - x;
//					 speed_diff -= diff;
					x += diff * x_direction;
					y += diff * y_direction;
				}
				if (last_y >= obj.ymax) {
					y_direction *= -1;
					diff = y - obj.ymax;
//					 speed_diff -= diff;
					x += diff * x_direction;
					y += diff * y_direction;
				}
				if (last_ymax <= obj.y) {
					y_direction *= -1;
					diff = obj.y - y;
//					 speed_diff -= diff;
					x += diff * x_direction;
					y += diff * y_direction;
				}

				if (!obj.destroy()) {
					bricks.remove(obj);
				}
				status = true;
				break;
			}
		}

		return status;
	}

	private void moveBall(double canTravelX, double canTravelY) {
		x += canTravelX;
		y += canTravelY;
	}

	public void reset() {
	//	firsttime = false;
		x_direction = 1;
		y_direction = -1;
	}
}