package com.walchand.game;

import com.walchand.gameinterface.Game;
import com.walchand.gameinterface.Screen;
import com.walchand.gameinterface.Graphics.PixmapFormat;

public class LoadingScreen extends Screen {


	public LoadingScreen(Game game) {
		super(game);
	}

	@Override
	public void update(float deltaTime) {
		
		
		Assets.background = game.getGraphics().newPixmap("back.png", PixmapFormat.RGB565);
		Assets.ball = game.getGraphics().newPixmap("ball_small.png", PixmapFormat.ARGB4444);
		Assets.bat = game.getGraphics().newPixmap("bat.png", PixmapFormat.ARGB4444);
		Assets.brick = game.getGraphics().newPixmap("bricks.png", PixmapFormat.ARGB4444);
		Assets.logo = game.getGraphics().newPixmap("logo.png", PixmapFormat.ARGB4444);
		Assets.menu = game.getGraphics().newPixmap("menu.jpg", PixmapFormat.RGB565);
		
		Assets.menu = game.getGraphics().newPixmap("menu.jpg", PixmapFormat.RGB565);
		Assets.sound = game.getGraphics().newPixmap("sound.png",PixmapFormat.ARGB4444);
		Assets.menuOpt = game.getGraphics().newPixmap("menuOpt.png",PixmapFormat.ARGB4444);
		
		Assets.pause = game.getGraphics().newPixmap("pause.png", PixmapFormat.ARGB4444);
		Assets.gameOver = game.getGraphics().newPixmap("gameOver.png", PixmapFormat.ARGB4444);
		Assets.ready = game.getGraphics().newPixmap("ready.png", PixmapFormat.ARGB4444);
		Assets.resume = game.getGraphics().newPixmap("resume.png", PixmapFormat.ARGB4444);
		Assets.congrats = game.getGraphics().newPixmap("congrats.png", PixmapFormat.ARGB4444);
		
		Assets.click= game.getAudio().newSound("click.ogg");
		
		Assets.start.add( game.getAudio().newSound("voice_start_01.ogg"));
		Assets.start.add( game.getAudio().newSound("voice_start_02.ogg"));
		Assets.start.add( game.getAudio().newSound("voice_start_03.ogg"));
		
		Assets.win.add( game.getAudio().newSound("voice_star_00a.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_00b.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_01a.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_01b.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_02a.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_02b.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_02c.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_03a.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_03b.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_03c.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_03d.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_03e.ogg"));
		Assets.win.add( game.getAudio().newSound("voice_star_03f.ogg"));
	
		Assets.fail.add( game.getAudio().newSound("voice_fail_01.ogg"));
		Assets.fail.add( game.getAudio().newSound("voice_fail_02.ogg"));
		

		Assets.collide = game.getAudio().newSound("bouncer.ogg");
		Assets.gamemusic = game.getAudio().newMusic("game_music.ogg");
		Assets.menumusic = game.getAudio().newMusic("menu_music.ogg");
		game.setScreen(new MainMenuScreen(game));
	
	}

	@Override
	public void present(float deltaTime) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
