package com.walchand.game;

import com.walchand.gameinterface.Input.TouchEvent;

public class Paddle extends ColloidalObjects {

	int x_mid;
	int dX;
	float speed_diff;
	private final static int PADDLE_WIDTH = 70;
	private final static int PADDLE_HEIGHT = 17;

	public Paddle(int x, int y, int gameWidth, int gameHeight) {
		super(x, y, PADDLE_WIDTH, PADDLE_HEIGHT, gameWidth, gameHeight);
		reset();
		
	}

	public void move(TouchEvent event, float deltaTime) {

		
		if (event.type != TouchEvent.TOUCH_DOWN) {

			if (event.y > y) { // Then Allow Move

				if (event.x < x_mid && x > 0) {
					speed_diff = speed * deltaTime;
					dX = (int) (speed_diff > x ? x : speed_diff);
					x -= dX;
					x_mid -= dX;
				}

				if (event.x > x_mid && x < boundaryX) {
					speed_diff = speed * deltaTime;
					dX = boundaryX - x;
					dX = (int) (speed_diff > (dX) ? (dX) : speed_diff);
					x += dX;
					x_mid += dX;
				}
				calculate_xmax();
			}

		}
	}
	
	void reset() {
		x_mid = x + (width >> 1);
	}
}
