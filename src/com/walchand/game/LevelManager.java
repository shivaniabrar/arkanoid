package com.walchand.game;

import java.io.IOException;
import java.io.InputStream;

import com.walchand.gameinterface.FileIO;
import com.walchand.gameinterface.Game;

public class LevelManager {

	String level = "level";
	String xml = ".xml";
	int current;
	LevelParser parser;
	FileIO fileIO;
	InputStream in;

	public LevelManager(Game game) {
		this.fileIO = game.getFileIO();
		parser = new LevelParser();
		setLevel();
	}

	void nextLevel() {
		current++;
		setLevel();
	}

	void prevLevel() {
		current--;
		setLevel();
	}
	
	void loadLevel(int level) {
			current = level;
			setLevel();
	}

	private void setLevel() {
		try {
			if (in != null)
				in.close();
			in = fileIO.readAsset(level + current + xml);
			parser.closeLevel();
			parser.openLevel(in);
		} catch (IOException e) {
			current = 0;
			setLevel();
			e.printStackTrace();
		}
	}
}
