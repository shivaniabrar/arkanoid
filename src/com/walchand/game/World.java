package com.walchand.game;

import java.util.ArrayList;
import java.util.List;

import com.walchand.gameinterface.Game;

public class World {

	// Objects
	MyBall ball;
	Paddle paddle;
	ArrayList<BRICK> bricks;
	// Ball properties
	int ball_speed = 2;
	private static int initial_ballx = 0;
	private static int initial_bally = 0;

	// Paddle properties
	int paddle_speed = 3;
	private static int initial_paddlex = 0;
	private static int initial_paddley = 0;

//	// FPS control
//	float totTime = 0;
//	static float time = 0.04f;

	// Level Manager
	LevelManager lm;

	// Game
	Game game;
	public static final short GameWin = 0;
	public static final short GameOver = 1;
	public static final short WallCollision = 2;
	public static final short PaddleCollision = 3;
	public static final short BrickCollision = 4;
	public static final short NoCollision = -1;
	public static final short GameLife = 5;

	public static short status;
	int WorldWidth;
	int WorldHeight;

	public World(Game game) {

		this.game = game;
		WorldWidth = game.getGraphics().getWidth();
		WorldHeight = game.getGraphics().getHeight();

		this.initial_ballx = WorldWidth >> 2;
		this.initial_bally = WorldHeight - 200;
		this.initial_paddlex = initial_ballx - 10;
		this.initial_paddley = WorldHeight - 100;

		ball = new MyBall(initial_ballx, initial_bally, WorldWidth, WorldHeight);
		paddle = new Paddle(initial_paddlex, initial_paddley, WorldWidth,
				WorldHeight);
		bricks = new ArrayList<BRICK>();

		lm = new LevelManager(game);
		lm.loadLevel(0);

		Settings.user.lives = 3;
		Settings.user.score = 0;
		status = NoCollision;
		ball.setSpeed(ball_speed);
		paddle.setSpeed(paddle_speed);
		placeBricks();
	}

	public void update(float deltaTime) {

		
		if (status != GameLife) {
			status = ball.move(paddle, bricks,deltaTime);
			if (status == GameOver) {
				Settings.user.lives--;
				if (Settings.user.lives != 0) {
					status = GameLife;
				}
			}
			if (bricks.size() == 0) {
				lm.nextLevel();
				placeBricks();
				status = GameWin;
				reset();
			}
		} else {
			status = NoCollision;
			reset();
		}

	}

	private void placeBricks() {
		lm.parser.getBricks(bricks);
	}

	List<BRICK> getBricks() {
		return bricks;
	}

	Paddle getPaddle() {
		return paddle;
		
	}

	
	void reset() {
		ball.setX(initial_ballx);
		ball.setY(initial_bally);
		paddle.setX(initial_paddlex);
		paddle.setY(initial_paddley);
		ball.reset();
		paddle.reset();
		paddle.calculate_xmax();
		ball.calculate_xmax();
	}
}