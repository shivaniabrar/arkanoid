package com.walchand.game;

import java.util.List;
import java.util.Random;

import android.graphics.Rect;

import com.walchand.gameinterface.Game;
import com.walchand.gameinterface.Graphics;
import com.walchand.gameinterface.Input.TouchEvent;
import com.walchand.gameinterface.Screen;

public class GameScreen extends Screen {

	enum GameState {
		GamePlay, GamePause, GameOver, GameReady, GameWin
	}

	World myWorld;
	List<BRICK> bricks;
	GameState state;
	Rect pause, resume, quit;

	Random choice;
	int resumex;
	int resumey;

	public GameScreen(Game game) {
		super(game);
		myWorld = new World(game);
		bricks = myWorld.getBricks();

		state = GameState.GameReady;
		choice = new Random();
		resumex = game.getGraphics().getWidth() >> 2;
		resumey = game.getGraphics().getHeight() >> 2;

		pause = new Rect(0, 0, 26, 36);
		resume = new Rect(resumex, resumey, resumex + 173, resumey + 28);
		quit = new Rect(resumex + 38, resumey + 38, resumex + 38 + 116,
				resumey + 63);

		if (Settings.sound) {
			Assets.gamemusic.play();
			Assets.start.get(choice.nextInt(3)).play(1);
			Assets.gamemusic.setLooping(true);
		}
	}

	@Override
	public void update(float deltaTime) {
		if (state == GameState.GamePlay)
			updatePlay(deltaTime);
		if (state == GameState.GamePause)
			updatePause(deltaTime);
		if (state == GameState.GameReady)
			updateReady(deltaTime);
		if (state == GameState.GameOver)
			updateGameOver(deltaTime);
		if (state == GameState.GameWin)
			updateGameWin(deltaTime);
		return;

	}

	private void updateGameWin(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		for (TouchEvent touchEvent : touchEvents) {
			if (touchEvent.type == TouchEvent.TOUCH_UP) {
				state = GameState.GamePlay;
				return;
			}
		}

	}

	private void updatePlay(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		for (TouchEvent touchEvent : touchEvents) {

			if (touchEvent.type != TouchEvent.TOUCH_UP) {
				myWorld.paddle.move(touchEvent,deltaTime);
			} else {

				if (inBounds(pause, touchEvent.x, touchEvent.y)) {
					state = GameState.GamePause;
					if (Settings.sound) {
						Assets.click.play(1);
					}
					return;
				}
			}

		}

		myWorld.update(deltaTime);

		if (myWorld.status == World.BrickCollision
				|| myWorld.status == World.PaddleCollision
				|| myWorld.status == World.WallCollision) {
			if (Settings.sound)
				Assets.collide.play(1);
		}

		if (myWorld.status == World.GameOver) {
			if (Settings.sound)
				Assets.fail.get(choice.nextInt(2)).play(1);

			state = GameState.GameOver;
		}
		if (myWorld.status == World.GameWin) {
			if (Settings.sound)
				Assets.win.get(choice.nextInt(13)).play(1);
			state = GameState.GameWin;
		}
	}

	private void updatePause(float deltaTime) {

		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		for (TouchEvent touchEvent : touchEvents) {

			if (inBounds(resume, touchEvent.x, touchEvent.y)) {

				if (Settings.sound) {
					Assets.click.play(1);
				}
				state = GameState.GamePlay;
				return;
			}

			if (inBounds(quit, touchEvent.x, touchEvent.y)) {
				if (Settings.sound) {
					Assets.gamemusic.pause();
				}
				game.setScreen(new MainMenuScreen(game));
				return;
			}

		}

	}

	private void updateGameOver(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		for (TouchEvent touchEvent : touchEvents) {
			if (touchEvent.type == TouchEvent.TOUCH_UP) {
				if (Settings.sound) {
					Assets.gamemusic.pause();
				}
				game.setScreen(new MainMenuScreen(game));
				return;
			}
		}

	}

	private void updateReady(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();

		for (TouchEvent touchEvent : touchEvents) {
			if (touchEvent.type == TouchEvent.TOUCH_UP) {
				state = GameState.GamePlay;
				return;
			}
		}

	}

	@Override
	public void present(float deltaTime) {
		Graphics graphics = game.getGraphics();
		graphics.drawPixmap(Assets.background, 0, 0);
		drawWorld(graphics);
		if (state == GameState.GamePlay)
			presentPlay(graphics);
		if (state == GameState.GamePause)
			presentPause(graphics);
		if (state == GameState.GameReady)
			presentReady(graphics);
		if (state == GameState.GameOver)
			presentOver(graphics);
		if (state == GameState.GameWin)
			presentWin(graphics);

	}

	private void presentWin(Graphics graphics) {
		graphics.drawPixmap(Assets.congrats, resumex >> 1, resumey);
	}

	private void presentReady(Graphics graphics) {
		graphics.drawPixmap(Assets.ready, resumex,resumey);
	}

	private void presentOver(Graphics graphics) {
		graphics.drawPixmap(Assets.gameOver, resumex,resumey);
	}

	private void presentPause(Graphics graphics) {
		graphics.drawPixmap(Assets.resume, resumex, resumey);
	}

	void presentPlay(Graphics graphics) {
		graphics.drawPixmap(Assets.pause, 0, 0);
	}

	void drawWorld(Graphics graphics) {

		// Draw Ball
		graphics.drawPixmap(Assets.ball, myWorld.ball.getX(),
				myWorld.ball.getY());

		// Draw Bat
		graphics.drawPixmap(Assets.bat, myWorld.paddle.getX(),
				myWorld.paddle.getY());

		for (BRICK brick : myWorld.getBricks()) {
			if (brick.available) {
				switch (brick.type) {
				case BRICK.type0:
					graphics.drawPixmap(Assets.brick, brick.getX(),
							brick.getY(), 0, 0, 46, 23);
					break;
				}
			}
		}
	}

	@Override
	public void pause() {
		if (Settings.sound)
			Assets.gamemusic.stop();
	}

	@Override
	public void resume() {
		// state = GameState.GameResume;
	}

	@Override
	public void dispose() {

	}

	boolean inBounds(Rect r, int x, int y) {
		return (x >= r.left && x <= r.right && y >= r.top && y <= r.bottom);
	}
}
