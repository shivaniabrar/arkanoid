package com.walchand.game;

public abstract class BRICK extends ArkanoidObjects{

	public BRICK(int x, int y, int width, int height) {
		super(x, y, width, height);
		available = true;
	}


	boolean available;
	int type;
	
	public static final short type0 = 0;
	
		
	abstract boolean destroy() ;
}
