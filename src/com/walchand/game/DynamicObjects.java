package com.walchand.game;


public abstract class DynamicObjects extends ArkanoidObjects {

	
	public DynamicObjects(int x, int y, int width, int height,int gameWidth,
			int gameHeight) {
		super(x, y, width, height);
	
		this.gameHeight = gameHeight-1;
		this.gameWidth = gameWidth-1;
	}

	protected int gameWidth;
	protected int gameHeight;

	int speed;

	int getSpeed() {
		return speed;
	}

	void setSpeed(int speed) {
		this.speed = speed;
	}

}
