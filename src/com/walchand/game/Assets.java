package com.walchand.game;


import java.util.ArrayList;

import com.walchand.gameinterface.Music;
import com.walchand.gameinterface.Pixmap;
import com.walchand.gameinterface.Sound;

public class Assets {

	
	public static Pixmap background;
	public static Pixmap ball;
	public static Pixmap bat;
	public static Pixmap brick;
	public static Pixmap logo;
	public static Pixmap menu;
	public static Pixmap sound;
	public static Pixmap menuOpt;
	public static Pixmap pause;
	public static Pixmap gameOver;
	public static Pixmap ready;
	public static Pixmap resume;
	public static Pixmap congrats;
	
	
	public static Sound click;
	public static Sound collide;
	public static Music gamemusic;
	public static Music menumusic;
	public static ArrayList<Sound> start = new ArrayList<Sound>();
	public static ArrayList<Sound> win = new ArrayList<Sound>();
	public static ArrayList<Sound> fail = new ArrayList<Sound>();
}
