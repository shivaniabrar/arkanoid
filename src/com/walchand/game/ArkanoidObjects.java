package com.walchand.game;

public abstract class ArkanoidObjects {

	protected int x;
	protected int y;

	protected int height;
	protected int width;

	protected int xmax;
	protected int ymax;
	
	public ArkanoidObjects(int x,int y,int width,int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		calculate_xmax();
		calculate_ymax();
	}
	
	
	void calculate_xmax() {
		xmax = x + width;
	}
	
	void calculate_ymax() {
		ymax = y + height;
	}
	
	int getX() {
		return x;
	}

	int getY() {
		return y;
	}

	void setX(int x) {
		this.x = x;
	}

	void setY(int y) {
		this.y = y;
	}

	int getHeight() {
		return height;
	}

	int getWidth() {
		return width;
	}

	void setHeight(int height) {
		this.height = height;
	}

	void setWidth(int width) {
		this.width = width;
	}

	boolean inBounds(ArkanoidObjects obj) {
		if(((x < obj.xmax) && (xmax > obj.x )) && ((y < obj.ymax) && (ymax > obj.y ))) {
				return true;
		}
		return false;
	}
	

}
