package com.walchand.game;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LevelParser {

	DocumentBuilder builder;
	Document doc;
	String level;

	public LevelParser() {
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void getBricks(ArrayList<BRICK> bricks) {
		NodeList brick = doc.getElementsByTagName("brick");

		String type;
		String x, y;
		for (int i = 0; i < brick.getLength(); i++) {
			type = ((Element) brick.item(i)).getAttribute("type");
			x = ((Element) brick.item(i)).getAttribute("x");
			y = ((Element) brick.item(i)).getAttribute("y");

			if (type.equals("NormalBrick")) {
				bricks.add(new NormalBrick(Integer.parseInt(x), Integer
						.parseInt(y), 0, 0));
			}
		}
	}

	void openLevel(InputStream in) {
		try {
			doc = builder.parse(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void closeLevel() {
		//builder.reset();
	}

}
