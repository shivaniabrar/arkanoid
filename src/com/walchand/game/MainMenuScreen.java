package com.walchand.game;

import java.util.List;

import android.R.color;
import android.graphics.Rect;

import com.walchand.gameinterface.Game;
import com.walchand.gameinterface.Graphics;
import com.walchand.gameinterface.Screen;
import com.walchand.gameinterface.Input.TouchEvent;

public class MainMenuScreen extends Screen {

	int logoX = 5;
	int logoY = 10;

	int menuOptX = logoX;
	int menuOptY = (logoY + 118) + 50; //logoY+logoH

	Rect play;
	Rect highScore;
	Rect sound;

	
	public MainMenuScreen(Game game) {
		super(game);
		play = new Rect(menuOptX + 89, menuOptY, menuOptX + 89 + 115,
				menuOptY + 27);
		highScore = new Rect(menuOptX, menuOptY + 38, menuOptX+290, menuOptY+63);
		sound = new Rect(0, game.getGraphics().getHeight() - 68, 64, game
				.getGraphics().getHeight());
		if (Settings.sound)
			Assets.menumusic.play();

	}

	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		for (TouchEvent touchEvent : touchEvents) {
			if (touchEvent.type == TouchEvent.TOUCH_UP) {
				
				if (inBounds(play, touchEvent.x, touchEvent.y)) {
					if (Settings.sound) 
						Assets.click.play(1);
					
					game.setScreen(new GameScreen(game));
					return;
				} 
				
				if (inBounds(highScore, touchEvent.x, touchEvent.y)) {
					if (Settings.sound) 
						Assets.click.play(1);
					game.setScreen(new GameScreen(game));
					return;
				}
				if (inBounds(sound, touchEvent.x, touchEvent.y)) {
					Settings.sound = !Settings.sound;
					if (Settings.sound) {
						Assets.click.play(1);
						Assets.menumusic.play();
					}else {
						Assets.menumusic.stop();
					}
				}
			}
		}

	}

	@Override
	public void present(float deltaTime) {
		Graphics g = game.getGraphics();
		g.clear(color.holo_blue_bright);
		g.drawPixmap(Assets.logo, logoX, logoY);
		g.drawPixmap(Assets.menuOpt, menuOptX, menuOptY);
		if (Settings.sound)
			g.drawPixmap(Assets.sound, 0, sound.top, 0, 0, 64, 68);
		else
			g.drawPixmap(Assets.sound, 0, sound.top, 67, 0, 64, 68);
	}

	@Override
	public void pause() {
		if (Settings.sound)
			Assets.menumusic.stop();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	boolean inBounds(Rect r, int x, int y) {
		return (x >= r.left && x <= r.right && y >= r.top && y <= r.bottom);
	}
}
