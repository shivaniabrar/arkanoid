package com.walchand.game;

public class NormalBrick extends BRICK{

	
	public static final int NoramlBrick_WIDTH=46;
	public static final int NoramlBrick_HEIGHT=23;
	
	public NormalBrick(int x, int y, int width, int height) {
		super(x, y, NoramlBrick_WIDTH, NoramlBrick_HEIGHT);
		type = type0;
	}

	@Override
	boolean destroy() {
		available = false;
		return available;
	}

}
