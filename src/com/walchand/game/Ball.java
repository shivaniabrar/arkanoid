package com.walchand.game;

import java.util.List;

public class Ball extends ColloidalObjects {

	// Temp Ball for prediction
	ArkanoidObjects newBall;

	// Constants
	public static final short changeDirection = -1;
	public static final short sameDirection = 1;

	private float x_direction_move;
	private float y_direction_move;
	private float x_direction = 1;
	private float y_direction = -1;

	private final static int BALL_WIDTH = 13;
	private final static int BALL_HEIGHT = 12;

	float deltaTime;
	// status
	short status;

	public Ball(int x, int y, int gameWidth, int gameHeight) {
		super(x, y, BALL_WIDTH, BALL_HEIGHT, gameWidth, gameHeight);
		status = World.NoCollision;
	}

	// Move Ball
	public short move(Paddle Paddle, List<BRICK> bricks,float deltaTime) {

		this.deltaTime = deltaTime;
		// Calculate next move
		if (status == World.NoCollision || (x_direction_move ==0  && y_direction_move ==0))
			mynextMove();

		// Create newBall if not created
		if (newBall == null)
			newBall = new Ball(x, y, gameWidth, gameHeight);

		// Set up tmpBall coordinates
		setnewBallXY();

		// Detect collision and do actions accordingly
		status = collisionDetect(Paddle, bricks);

		// No collision move ball normally
		if (status == World.NoCollision) {
			moveBall(x_direction_move, y_direction_move);
			if (ymax >= gameHeight)
				status = World.GameOver;
		}

		return status;
	}

	// Calculate next move
	private void mynextMove() {
		x_direction_move = x_direction * speed  ;
		y_direction_move = y_direction * speed  ;
	}

	// Setup coordinates of newBall
	private void setnewBallXY() {
		newBall.x = (int) ((int) x + x_direction_move);
		newBall.y = (int) ((int) y + y_direction_move);
		newBall.calculate_xmax();
		newBall.calculate_ymax();
	}

	// Detect collision
	private short collisionDetect(Paddle Paddle, List<BRICK> bricks) {

		// Calculate xmax & ymax
		calculate_xmax();
		calculate_ymax();

		if (WallCollision())
			return World.WallCollision;
		if (PaddleCollision(Paddle))
			return World.PaddleCollision;
		if (BrickCollision(bricks))
			return World.BrickCollision;

		return World.NoCollision;
	}

	private boolean WallCollision() {
		if (newBall.x >= boundaryX || (newBall.y) <= 0 || newBall.x <= 0) {
			reboundWall();
			return true;
		}
		return false;
	}

	private boolean PaddleCollision(Paddle obj) {
		if (newBall.inBounds(obj)) {
			reboundPaddle(obj);
			return true;
		}
		return false;
	}

	private boolean BrickCollision(List<BRICK> bricks) {
		boolean status = false;

		for (BRICK b : bricks) {

			if (newBall.inBounds(b)) {
				reboundBrick(b);
				if (!b.destroy()) {
					bricks.remove(b);
				}
				status = true;
				break;
			}
		}
		return status;
	}

	private void reboundWall() {
		float limitY = gameHeight;
		float limitX = gameWidth;

		if (x_direction_move < 0.0) // Left
			limitX = 0;
		if (y_direction_move < 0.0) // UP
			limitY = 0;

		rebound(limitX, limitY, boundaryX, boundaryY);
	}

	private void reboundPaddle(Paddle obj) {

		float limitXW;
		float limitYH;
		float limitX = obj.x;
		float limitY = obj.y;

		if (x_direction_move < 0)
			limitX = obj.xmax; // <--- Ball Direction
		if (y_direction_move < 0)
			limitY = obj.ymax; // ^--- BALL Direction

		limitXW = limitX - width;
		limitYH = limitY - height;

		rebound(limitX, limitY, limitXW, limitYH);
	}

	private void reboundBrick(BRICK b) {
		float limitXW;
		float limitYH;
		float limitX;
		float limitY;

		limitX = b.x;
		limitY = b.y;

		if (x_direction_move < 0)
			limitX = b.xmax; // <--- Ball Direction
		if (y_direction_move < 0)
			limitY = b.ymax; // ^--- BALL Direction

		limitXW = limitX - width;
		limitYH = limitY - height;
		rebound(limitX, limitY, limitXW, limitYH);
	}

	private void rebound(float limitX, float limitY, float limitXW,
			float limitYH) {

		if (x_direction_move > 0.0) {

			double canTravelX = limitXW - x;
			double canTravelY = (canTravelX / x_direction_move)
					* y_direction_move;
			double canTravelY2 = checkY(newBall.y, limitY);

			if (canTravel(canTravelY, canTravelY2)) {
				moveBall(canTravelX, canTravelY);
				changeDirection(changeDirection, sameDirection);
				return;
			}
		}

		// Move Right
		if (x_direction_move < 0.0) {
			double canTravelX = (limitX - x);
			double canTravelY = (canTravelX / x_direction_move)
					* y_direction_move;
			double canTravelY2 = checkY(newBall.y, limitY);

			if (canTravel(canTravelY, canTravelY2)) {
				moveBall(canTravelX, canTravelY);
				changeDirection(changeDirection, sameDirection);
				return;
			}
		}

		// Move Up
		if (y_direction_move > 0.0) {

			double canTravelY = limitYH - y;
			double canTravelX = (canTravelY / y_direction_move)
					* x_direction_move;
			moveBall(canTravelX, canTravelY);
			changeDirection(sameDirection, changeDirection);
			return;
		}

		// Move Down
		if (y_direction_move < 0.0) {
			double canTravelY = (limitY - y);
			double canTravelX = (canTravelY / y_direction_move)
					* x_direction_move;
			moveBall(canTravelX, canTravelY);
			changeDirection(sameDirection, changeDirection);
			return;
		}

	}

	private void moveBall(double canTravelX, double canTravelY) {
		x += canTravelX;
		y += canTravelY;
		x_direction_move -= canTravelX; // if !collision then will be 0
		y_direction_move -= canTravelY;
	}

	private double checkY(double newY, double limitY) {
		double canTravelY2 = y_direction_move;

		if (y_direction_move > 0.0 && newY >= limitY - height) {
			canTravelY2 = limitY - height - y;
		}
		if (y_direction_move < 0.0 && newY < limitY) {
			canTravelY2 = limitY - y;
		}
		return canTravelY2;
	}

	private boolean canTravel(double canTravelY, double canTravelY2) {
		return Math.abs(canTravelY2) >= Math.abs(canTravelY);
	}

	private void changeDirection(int x_direction, int y_direction) {
		this.x_direction_move *= x_direction;
		this.y_direction_move *= y_direction;
		this.x_direction *= x_direction;
		this.y_direction *= y_direction;
	}

	void reset() {
		x_direction = 1;
		y_direction = -1;
	}
}