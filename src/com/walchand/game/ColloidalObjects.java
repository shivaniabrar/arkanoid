package com.walchand.game;


public abstract class ColloidalObjects  extends DynamicObjects{

	
	protected int boundaryX;
	protected int boundaryY;
	
	public ColloidalObjects(int x, int y, int width, int height, int gameWidth,
			int gameHeight) {
		super(x, y, width, height, gameWidth, gameHeight);
		setBoundaryX();
		setBoundaryY();
	}

	private void setBoundaryX() {
		this.boundaryX = gameWidth - width;
	}

	private void setBoundaryY() {
		this.boundaryY = gameHeight - height;
	}

	int getBoundaryX() {
		return boundaryX;
	}

	int getBoundaryY() {
		return boundaryY;
	}
	
}
